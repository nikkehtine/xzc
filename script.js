// Dark mode toggle
let darkEnabled = localStorage.getItem("darkEnabled");
const dmSwitch = document.querySelector("#darkmode-toggle");

const enableDark = () => {
  document.body.classList.add("dark");
  document.body.style.colorScheme = "dark";
  dmSwitch.checked = true;
  localStorage.setItem("darkEnabled", "true");
};

const disableDark = () => {
  document.body.classList.remove("dark");
  document.body.style.colorScheme = "light";
  dmSwitch.checked = false;
  localStorage.setItem("darkEnabled", "false");
};

if (darkEnabled == "true") {
  enableDark();
}

dmSwitch.addEventListener("change", function () {
  dmSwitch.checked ? enableDark() : disableDark();
});
